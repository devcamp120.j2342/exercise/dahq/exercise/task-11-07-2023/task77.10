package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Customer;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.repository.PaymentRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class PaymentController {
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/payment")
    public List<Payment> getAllPayment() {
        return paymentRepository.findAll();

    }

    @GetMapping("/payment/{id}")
    public Payment getPaymentById(@PathVariable("id") int id) {
        return paymentRepository.findById(id).get();
    }

    @PostMapping("/customer/{id}/payment")
    public ResponseEntity<Payment> createPayment(@PathVariable("id") int id, @RequestBody Payment pPayment) {
        try {
            Optional<Customer> customerData = customerRepository.findById(id);
            if (customerData.isPresent()) {
                Payment paymentData = new Payment();
                paymentData.setAmmount(pPayment.getAmmount());
                paymentData.setCheckNumber(pPayment.getCheckNumber());
                paymentData.setCustomer(customerData.get());
                paymentData.setPaymentDate(new Date());
                return new ResponseEntity<Payment>(paymentRepository.save(paymentData), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/payment/{id}")
    public ResponseEntity<Payment> updatePayment(@PathVariable("id") int id, @RequestBody Payment pPayment) {
        try {
            Optional<Payment> paymentData = paymentRepository.findById(id);
            if (paymentData.isPresent()) {
                paymentData.get().setPaymentDate(new Date());
                paymentData.get().setCheckNumber(pPayment.getCheckNumber());
                paymentData.get().setAmmount(pPayment.getAmmount());

                return new ResponseEntity<>(paymentRepository.save(paymentData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/payment/{id}")
    public ResponseEntity<Payment> deletePayment(@PathVariable("id") int id) {
        paymentRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @GetMapping("/payment-fullname")
    public ResponseEntity<List<Map<String, Object>>> findPayment() {
        List<Object[]> results = paymentRepository.findPayment();
        List<Map<String, Object>> fullName = new ArrayList<>();
        for (Object[] result : results) {
            Map<String, Object> map = new HashMap<>();
            map.put("check_number", result[0]);
            map.put("payment_date", result[1]);
            map.put("ammount", result[2]);
            map.put("full_name", result[3]);
            fullName.add(map);
        }
        return ResponseEntity.ok(fullName);
    }

}

package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.pizza365.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    @Query(value = "SELECT COUNT(country) as c_cou, country FROM customers GROUP BY country", nativeQuery = true)
    List<Object[]> findCustomerCountByCountry();

}

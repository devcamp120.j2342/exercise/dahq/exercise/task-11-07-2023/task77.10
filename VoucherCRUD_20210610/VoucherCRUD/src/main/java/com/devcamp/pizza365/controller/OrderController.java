package com.devcamp.pizza365.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.CustomerRepository;
import com.devcamp.pizza365.repository.OrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OrderController {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/order")
    public List<Order> getAllOrder() {
        return orderRepository.findAll();
    }

    @GetMapping("/order/{id}")
    public Order getOrderById(@PathVariable("id") int id) {
        Optional<Order> orderData = orderRepository.findById(id);
        return orderData.get();
    }

    @PostMapping("/customer/{id}/order")
    public ResponseEntity<Order> createOrder(@PathVariable("id") int id, @RequestBody Order pOrder) {
        try {
            Optional<Customer> customerData = customerRepository.findById(id);
            if (customerData.isPresent()) {
                Order orderData = new Order();
                orderData.setOrderDate(new Date());
                orderData.setRequiredDate(new Date());
                orderData.setShippedDate(new Date());
                orderData.setStatus(pOrder.getStatus());
                orderData.setComments(pOrder.getComments());
                orderData.setCustomer(customerData.get());
                orderData.setOrderDetails(pOrder.getOrderDetails());
                return new ResponseEntity<>(orderRepository.save(orderData), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/order/{id}")
    public ResponseEntity<Order> updateOrder(@PathVariable("id") int id, @RequestBody Order pOrder) {
        try {
            Optional<Order> orderData = orderRepository.findById(id);
            if (orderData.isPresent()) {
                orderData.get().setOrderDate(new Date());
                orderData.get().setRequiredDate(new Date());
                orderData.get().setShippedDate(new Date());
                orderData.get().setStatus(pOrder.getStatus());
                orderData.get().setComments(pOrder.getComments());
                orderData.get().setCustomer(pOrder.getCustomer());
                // orderData.get().setOrderDetails(pOrder.getOrderDetails());
                return new ResponseEntity<>(orderRepository.save(orderData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/order/{id}")
    public ResponseEntity<Order> deleteOrder(@PathVariable("id") int id) {
        orderRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

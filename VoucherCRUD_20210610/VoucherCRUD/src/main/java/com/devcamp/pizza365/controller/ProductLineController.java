package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.ProductLineRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProductLineController {
    @Autowired
    ProductLineRepository productLineRepository;

    @GetMapping("/productLine")
    public List<ProductLine> getAllProducLine() {
        return productLineRepository.findAll();
    }

    @GetMapping("/productLine/{id}")
    public ProductLine getProductLineById(@PathVariable("id") Integer id) {
        return productLineRepository.findById(id).get();
    }

    @PostMapping("/productLine")
    public ResponseEntity<ProductLine> createProductLine(@RequestBody ProductLine pProductLine) {
        try {
            ProductLine productLineData = new ProductLine();
            productLineData.setProductLine(pProductLine.getProductLine());
            productLineData.setDescription(pProductLine.getDescription());
            productLineData.setProducts(pProductLine.getProducts());
            return new ResponseEntity<>(productLineRepository.save(productLineData), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/productLine/{id}")
    public ResponseEntity<ProductLine> updateProductLine(@PathVariable("id") Integer id,
            @RequestBody ProductLine pProductLine) {
        try {
            Optional<ProductLine> productLineData = productLineRepository.findById(id);
            if (productLineData.isPresent()) {
                productLineData.get().setProductLine(pProductLine.getProductLine());
                productLineData.get().setDescription(pProductLine.getDescription());
                return new ResponseEntity<>(productLineRepository.save(productLineData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/productLine/{id}")
    public ResponseEntity<ProductLine> deleteProductLine(@PathVariable("id") Integer id) {
        productLineRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

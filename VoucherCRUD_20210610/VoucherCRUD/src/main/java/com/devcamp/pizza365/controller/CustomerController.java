package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.repository.*;
import com.devcamp.pizza365.service.ExcelExporter;

import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
	@Autowired
	CustomerRepository customerRep;
	@Autowired
	OrderRepository orderRep;
	@Autowired
	OrderDetailRepository orderDetailRep;

	@GetMapping("/allCustomers")
	public ResponseEntity<Object> getAllCustomers() {
		try {
			List<Customer> customer = new ArrayList<Customer>();

			customerRep.findAll().forEach(customer::add);

			return new ResponseEntity<>(customer, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/allOrders")
	public ResponseEntity<Object> getAllOrders() {
		try {
			List<Order> orders = new ArrayList<Order>();

			orderRep.findAll().forEach(orders::add);

			return new ResponseEntity<>(orders, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			// return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/allOrderDetails")
	public ResponseEntity<Object> getAllOrderDetails() {
		try {
			List<OrderDetail> orderDetails = new ArrayList<OrderDetail>();

			orderDetailRep.findAll().forEach(orderDetails::add);

			return new ResponseEntity<>(orderDetails, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/export/customers/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRep.findAll().forEach(customer::add);
		ExcelExporter excelExporter = new ExcelExporter(customer);
		excelExporter.export(response);
	}

	@GetMapping("/count-by-country")
	public ResponseEntity<List<Map<String, Object>>> findCustomerCountByCountry() {
		List<Object[]> results = customerRep.findCustomerCountByCountry();
		List<Map<String, Object>> countByCountry = new ArrayList<>();
		for (Object[] result : results) {
			Map<String, Object> map = new HashMap<>();
			map.put("count", result[0]);
			map.put("country", result[1]);
			countByCountry.add(map);
		}
		return ResponseEntity.ok(countByCountry);
	}

	@PostMapping("/customer")
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer pCustomer) {
		try {
			Customer customerData = new Customer();
			customerData.setAddress(pCustomer.getAddress());
			customerData.setCity(pCustomer.getCity());
			customerData.setCountry(pCustomer.getCountry());
			customerData.setCreditLimit(pCustomer.getCreditLimit());
			customerData.setFirstName(pCustomer.getFirstName());
			customerData.setLastName(pCustomer.getLastName());
			customerData.setPhoneNumber(pCustomer.getPhoneNumber());
			customerData.setPostalCode(pCustomer.getPostalCode());
			customerData.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
			customerData.setOrders(pCustomer.getOrders());
			return new ResponseEntity<>(customerRep.save(customerData),
					HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/customer/{id}")
	public ResponseEntity<Customer> updateCustomer(@PathVariable("id") int id, @RequestBody Customer pCustomer) {
		try {
			Optional<Customer> customerData = customerRep.findById(id);
			if (customerData.isPresent()) {
				customerData.get().setAddress(pCustomer.getAddress());
				customerData.get().setCity(pCustomer.getCity());
				customerData.get().setCountry(pCustomer.getCountry());
				customerData.get().setCreditLimit(pCustomer.getCreditLimit());
				customerData.get().setFirstName(pCustomer.getFirstName());
				customerData.get().setLastName(pCustomer.getLastName());
				customerData.get().setPhoneNumber(pCustomer.getPhoneNumber());
				customerData.get().setPostalCode(pCustomer.getPostalCode());
				customerData.get().setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
				return new ResponseEntity<>(customerRep.save(customerData.get()), HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/customer/{id}")
	public ResponseEntity<Customer> deleteCustomer(@PathVariable("id") int id) {
		customerRep.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/customer/{id}")
	public Customer getCustomerById(@PathVariable("id") int id) {
		return customerRep.findById(id).get();
	}

	@GetMapping("/hello")
	public String hello() {
		return "hello";
	}

}

package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.OfficeRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OfficeController {
    @Autowired
    OfficeRepository officeRepository;

    @GetMapping("/office")
    public List<Office> getAllOffice() {
        return officeRepository.findAll();
    }

    @GetMapping("/office/{id}")
    public Office getOfficeById(@PathVariable("id") int id) {
        return officeRepository.findById(id).get();
    }

    @PostMapping("/office")
    public ResponseEntity<Office> createOffice(@RequestBody Office pOffice) {
        try {
            Office officeData = new Office();
            officeData.setCity(pOffice.getCity());
            officeData.setPhone(pOffice.getPhone());
            officeData.setAddressLine(pOffice.getAddressLine());
            officeData.setState(pOffice.getState());
            officeData.setCountry(pOffice.getCountry());
            officeData.setTerritory(pOffice.getTerritory());
            return new ResponseEntity<>(officeRepository.save(officeData), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/office/{id}")
    public ResponseEntity<Office> updateOffice(@PathVariable("id") Integer id, @RequestBody Office pOffice) {
        try {
            Optional<Office> officeData = officeRepository.findById(id);
            if (officeData.isPresent()) {
                officeData.get().setCity(pOffice.getCity());
                officeData.get().setPhone(pOffice.getPhone());
                officeData.get().setAddressLine(pOffice.getAddressLine());
                officeData.get().setState(pOffice.getState());
                officeData.get().setCountry(pOffice.getCountry());
                officeData.get().setTerritory(pOffice.getTerritory());
                return new ResponseEntity<>(officeRepository.save(officeData.get()), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/office/{id}")
    public ResponseEntity<Office> deleteOffice(@PathVariable("id") Integer id) {
        officeRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.ProductLineRepository;
import com.devcamp.pizza365.repository.ProductRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProductController {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductLineRepository productLineRepository;

    @GetMapping("/product")
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable("id") int id) {
        return productRepository.findById(id).get();
    }

    @PostMapping("/productLine/{id}/product")
    public ResponseEntity<Product> createProduct(@PathVariable("id") int id, @RequestBody Product pProduct) {
        try {
            Optional<ProductLine> productLineData = productLineRepository.findById(id);
            if (productLineData.isPresent()) {
                Product productData = new Product();
                productData.setProductCode(pProduct.getProductCode());
                productData.setProductName(pProduct.getProductName());
                productData.setProductDescription(pProduct.getProductDescription());
                productData.setProductScale(pProduct.getProductScale());
                productData.setProductVendor(pProduct.getProductVendor());
                productData.setQuantityInStock(pProduct.getQuantityInStock());
                productData.setBuyPrice(pProduct.getBuyPrice());
                productData.setProductLine(productLineData.get());
                return new ResponseEntity<>(productRepository.save(productData), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") int id, @RequestBody Product pProduct) {
        try {
            Optional<Product> productData = productRepository.findById(id);
            if (productData.isPresent()) {
                productData.get().setProductCode(pProduct.getProductCode());
                productData.get().setProductName(pProduct.getProductName());
                productData.get().setProductDescription(pProduct.getProductDescription());
                productData.get().setProductScale(pProduct.getProductScale());
                productData.get().setProductVendor(pProduct.getProductVendor());
                productData.get().setQuantityInStock(pProduct.getQuantityInStock());
                productData.get().setBuyPrice(pProduct.getBuyPrice());

                return new ResponseEntity<>(productRepository.save(productData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable("id") int id) {
        productRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

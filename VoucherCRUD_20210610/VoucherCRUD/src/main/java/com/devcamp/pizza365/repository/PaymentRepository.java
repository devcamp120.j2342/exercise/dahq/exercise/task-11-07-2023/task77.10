package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.pizza365.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {
    @Query(value = "SELECT payments.check_number, payments.payment_date,payments.ammount, CONCAT_WS(customers.first_name, customers.last_name) as full_name FROM payments INNER JOIN customers ON payments.customer_id=customers.id", nativeQuery = true)
    List<Object[]> findPayment();
}

package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.EmployeeRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/employee")
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employee/{id}")
    public Employee getEmployeeById(@PathVariable("id") int id) {
        return employeeRepository.findById(id).get();
    }

    @PostMapping("/employee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee pEmployee) {
        try {
            Employee employeeData = new Employee();
            employeeData.setLastName(pEmployee.getLastName());
            employeeData.setFirstName(pEmployee.getFirstName());
            employeeData.setExtension(pEmployee.getExtension());
            employeeData.setEmail(pEmployee.getEmail());
            employeeData.setOfficeCode(pEmployee.getOfficeCode());
            employeeData.setReportTo(pEmployee.getReportTo());
            employeeData.setJobTitle(pEmployee.getJobTitle());
            return new ResponseEntity<>(employeeRepository.save(employeeData), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") int id, @RequestBody Employee pEmployee) {
        try {
            Optional<Employee> employeeData = employeeRepository.findById(id);
            employeeData.get().setLastName(pEmployee.getLastName());
            employeeData.get().setFirstName(pEmployee.getFirstName());
            employeeData.get().setExtension(pEmployee.getExtension());
            employeeData.get().setEmail(pEmployee.getEmail());
            employeeData.get().setOfficeCode(pEmployee.getOfficeCode());
            employeeData.get().setReportTo(pEmployee.getReportTo());
            employeeData.get().setJobTitle(pEmployee.getJobTitle());
            return new ResponseEntity<>(employeeRepository.save(employeeData.get()), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") int id) {
        employeeRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
